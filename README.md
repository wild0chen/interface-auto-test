# HTTP接口测试
## 系统结构
.  
├── README.md  
├── log4j.properties  
├── pom.xml  
├── src  
│   ├── main  
│   │    └── java  
│   │        ├── base
│   │        ├── tool  
│   │        └── util   //工具类  
│   └── test  
│       ├── java  
│       │    ├── login  //登录测试demo
│       │    └── online //查询接口demo
│       └── resources   //bean xml、DB、MQ配置文件等


## 使用规范

* 克隆此代码仓库


* 创建个人test分支

```
git checkout -b xxx-test
```

* 改动代码，提交改动并push到个人test分支

```
git pull
git merge orign/master
git add -A
git commit -m "<your commit message>"
git push origin xxx-test
```
## 说明
### 一. 文件目录格式说明（以test.java.login为例子）
#### 1.用例保存目录有结构要求
- 所有的case都在test.java目录下
- 测试脚本必须保存在test.java.login.testsuites文件下，名字任意如TestLogin.java
- 数据文件必须放在test.java.login.data下以TestLogin.json的名字存在，即数据文件必须与测试脚本同名
#### 2.json数据结构规范
- json以数组的形式存在。
- 数组总每个 *{}* 包含的内容代表一个完整的测试用例。
- 每条用例都固定包含5个字段，如下：
  ```
  {
  "comments":"本用例/步骤的描述"，
  "method":"post/get",
  "url":"www.baidu.com/search",
  "response":{
    "p1": 123,
    "p2": "abc"
    //"form":{}, //form表单数据 可能会这样改
    //"query":{} //url参数
    }
  "response":{} //验证点数据
  }
  ```
- 每条测试用例中可以有多个步骤，用 *step* 字段来表示分步，如下：
  ```
  {
  "comments:"本用例描述",
  "step":[...]
  }
  ```
- 如果不写step则代表本条用例只有一步, 每step的数据结构和上面示例一样
#### 4.脚本编写
- 所有的类均派生于BaseTemplate类
- BaseTemplate类是一个抽象类，仅抽象了一个 *testCase* 方法，目的就是为了统一定义接口，实现它并在它上面添加 *@Test*
- 真正的执行在方法doIt中，此方法已定义了执行的流程
- doIt方法有4个参数分别代表了0.步骤是参数 1.CASE描述 2.每步执行完成的后处理, 3.注册后续操作(未实现)
- 参数3的后处理，可以不写，但是必须是连续的，如有：步骤【1，2，3，4】，其中步骤2、3有交互关系，那么就应该自定义一个交互函数，但是1，2之间没有关系，可以使用基类中的analyzerNullStep作为填充
- 简单说实现testCase并调用doIt即可

#### 5.自定义json
- 通过派生BaseParameter实现

#### 6.未实现功能
1.需要一个本地SQLite，记录一些数据
2.需要定时任务（延迟队列？@Scheduled?看吧）



- ***带query有带form的暂时还没考虑, 后面在看情况在决定是不是要拆开和上面示例中注释那样，哈哈哈***
