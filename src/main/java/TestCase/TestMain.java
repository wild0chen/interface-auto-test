package TestCase;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;


@Test(threadPoolSize = 2, invocationCount = 3)
@ContextConfiguration(locations = {"classpath:test-config.xml"})
public class TestMain extends AbstractTestNGSpringContextTests {
    public Logger logger = LogManager.getLogger();

    @Test()
    public void testDemo(){
        logger.info("Test 12345");
    }

    @Test
    public void testDemo2(){
        logger.info("Test222 12345");
    }
}
