package utils;

import cn.hutool.core.util.StrUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

public class PropertyReader {
    private static Logger logger = LogManager.getLogger(PropertyReader.class);

    private final Properties properties;

    public PropertyReader(String propFile) {
        if (StrUtil.isEmpty(propFile)){
            throw new NullPointerException("propFile must be not NULL!");
        }

        this.properties = new Properties();
        try {
            InputStream is = PropertyReader.class.getClassLoader().getResourceAsStream(propFile);
            properties.load(is);
        } catch (Exception ex) {
            logger.error("Not found {},msg: {}", propFile, ex.getMessage(), ex);
        }
    }

    public PropertyReader(File file) {
        if (file == null){
            throw new NullPointerException("File must be not NULL!");
        }

        properties = new Properties();
        try {
            InputStream is = new FileInputStream(file);
            properties.load(is);
        } catch (Exception ex) {
            logger.error("Not found {},msg: {}", file.getName(), ex.getMessage(), ex);
        }
    }

    public int getValueAsInt(String key, int defaultValue) {
        int value = defaultValue;
        String v = this.properties.getProperty(key);
        if (v != null) {
            try {
                value = Integer.parseInt(v.trim());
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
            }
        }

        return value;
    }

    public boolean getValueAsBool(String key, boolean defaultValue) {
        boolean value = defaultValue;
        String v = this.properties.getProperty(key);
        if (v != null) {
            try {
                value = Boolean.parseBoolean(v.trim());
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
            }
        }

        return value;
    }

    public String getValueAsString(String key, String defaultValue) {
        return this.properties.getProperty(key, defaultValue);
    }

    public long getValueAsLong(String key, long defaultValue) {
        long value = defaultValue;
        String v = this.properties.getProperty(key);
        if (v != null) {
            try {
                value = Long.parseLong(v.trim());
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
            }
        }

        return value;
    }

    public Set<String> stringPropertyNames() {
        return this.properties.stringPropertyNames();
    }

    public int getValueAsInt(String key) {
        return this.getValueAsInt(key, 0);
    }

    public boolean getValueAsBool(String key) {
        return this.getValueAsBool(key, false);
    }

    public String getValueAsString(String key) {
        return this.properties.getProperty(key);
    }

    public long getValueAsLong(String key) {
        return this.getValueAsLong(key, 0L);
    }
}
