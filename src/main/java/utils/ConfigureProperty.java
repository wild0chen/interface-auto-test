package utils;

import cn.hutool.core.util.StrUtil;
import lombok.Getter;

/**
 * 里面保存基础数据如token 基础hostURL
 */
public class ConfigureProperty {
    private Configs cfg;
    @Getter
    private String account;
    private String hostKey;

    public ConfigureProperty(String account, String hostKey){
        cfg = new Configs();
        this.account = account;
        this.hostKey = hostKey;
    }

    public ConfigureProperty(){
        cfg = new Configs();
        this.account = "defaultAccount";
        this.hostKey = "defaultHost";
    }

    public String getHost() throws Exception {
        return getHost(hostKey);
    }

    public String getHost(String hostKey) throws Exception {
        String hostUrl = cfg.getBaseHost(hostKey);
        if (StrUtil.isEmpty(hostUrl))
            throw new Exception(hostKey + ":在env.properties中找不到这个键");

        return hostUrl;
    }

    public Configs getCfg() {
        return cfg;
    }

    public static void main(String[] args) {
        ConfigureProperty login = new ConfigureProperty();
//        System.out.println(login.getToken());
    }
}