package utils;


import cn.hutool.core.util.StrUtil;
import lombok.Setter;

import java.io.File;

/**
 * 默认读取test下的env。properties文件
 * 在这个文件中保存的一些base host 的地址
 * 或者保存一些其他参数
 */
public final class Configs {
    //env.properties
    private PropertyReader propertyReader;
    @Setter
    private String propertyFileName = "env.properties";

    public Configs() {
        propertyReader = new PropertyReader(propertyFileName);
    }

    public Configs(String propertyFilePath) {
        if (StrUtil.isEmpty(propertyFilePath)){
            propertyFilePath = this.propertyFileName;
        }
        propertyReader = new PropertyReader(propertyFilePath);
    }

    public Configs(File propertyFile){
        if (propertyFile == null){
            throw new NullPointerException("propertyFile must be not NULL!");
        }
        propertyReader = new PropertyReader(propertyFile);
    }

    public String getBaseHost(String hostName){
        return getString(hostName);
    }

    public String getString(String key) {
        return getValue(key);
    }

    public int getInt(String key, int defaultvalue) {
        return propertyReader.getValueAsInt(key, defaultvalue);
    }

    public String getValue(String key) {
        String value = System.getenv(key);
        if (value == null && !key.isEmpty()) {
            value = propertyReader.getValueAsString(key);
        }

        return value;
    }

    public String setTestData() {
        String temp = propertyReader.getValueAsString("testData");
        if (StrUtil.isEmpty(temp)){
            temp = null;
        }

        return temp;
    }
}
