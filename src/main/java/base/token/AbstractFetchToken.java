package base.token;

public abstract class AbstractFetchToken {
    private String pwd;
    private String account;
    private String host;
    private String smsUrl;
    private String loginUrl;
    private String token;

    public AbstractFetchToken(String account){
        this.account = account;
    }

    //初始化短信模式
    public void initFromSms(String loginUrl, String smsUrl){
        this.loginUrl = loginUrl;
        this.smsUrl = smsUrl;
        this.token = getTokenFromSms();
    }

    //初始化密码模式
    public void initFromPassword(String loginUrl, String pwd){
        this.loginUrl = loginUrl;
        this.pwd = pwd;
        this.token = getTokenFromPassword();
    }

    public String getToken() {
        return token;
    }

    public abstract String getTokenFromSms();
    public abstract String getTokenFromPassword();
}
