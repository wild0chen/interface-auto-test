package base.vo;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

/**
 * 保存单次CASE的数据
 * 注意：在新增这里的属性时，只能在后面添加，不可以修改顺序，因为这里的顺序和base.JsonFieldDefinition是有关系的
 */
@Data
public class BaseParameter2 {
    //默认包含5个字段 0.请求参数。1.期望值。2.调用方法。3.URL，4.描述 5.与我交互的步骤的index
    public BaseParameter2(JSONObject request, JSONObject expected,
                          String method, String url, String step, JSONObject headers, Integer index){
        this.request = request;
        this.expected = expected;
        this.method = method;
        this.url = url;
        this.step = step;
        this.willIndex = index;
        if (headers != null)
            this.headers = headers.getInnerMap().entrySet().stream().collect(
                    Collectors.<Entry<String, Object>, String, String>toMap(x -> x.getKey(), x -> (String)x.getValue()));

    }
    private JSONObject request;
    private JSONObject expected;
    private Map<String, String> headers;
    private String method;
    private String url;
    private String step;

    @Getter
    private Integer willIndex;

    //保存调用结果
    @Getter
    @Setter
    private String httpResponse;

    @Override
    public String toString() {
        return "BaseParamter{" +
                "request=" + request +
                ", response=" + expected +
                ", method=" + method +
                ", url=" + url +
                ", comment=" + step +
                ", headers=" + headers +
                '}';
    }
}


