package base.originBase;

import cn.hutool.Hutool;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 保存单次CASE的数据
 * 注意：在新增这里的属性时，只能在后面添加，不可以修改顺序，因为这里的顺序和base.JsonFieldDefinition是有关系的
 */
@Data
public class BaseParameter {
    //默认包含5个字段0.请求参数。1.返回值。2.调用方法。3.URL，4.描述
    public BaseParameter(JSONObject request, JSONObject response, String method, String url, String step, JSONObject headers){
        this.request = request;
        this.response = response;
        this.method = method;
        this.url = url;
        this.step = step;
        if (headers != null)
            this.headers = headers.getInnerMap().entrySet().stream().collect(
                    Collectors.<Map.Entry<String, Object>, String, String>toMap(x -> x.getKey(), x -> (String)x.getValue()));

    }
    private JSONObject request;
    private JSONObject response;
    private Map<String, String> headers;
    private String method;
    private String url;
    private String step;

    @Override
    public String toString() {
        return "BaseParamter{" +
                "request=" + request +
                ", response=" + response +
                ", method=" + method +
                ", url=" + url +
                ", comment=" + step +
                ", headers=" + headers +
                '}';
    }
}


