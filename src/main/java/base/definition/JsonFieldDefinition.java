package base.definition;

public class JsonFieldDefinition {
    //Json 字段
    public final String JSON_STEP = "step";
    public final String JSON_COMMENTS = "comments";
    public final String JSON_URL = "url";
    public final String JSON_METHOD = "method";
    public final String JSON_REQUEST = "request";
    public final String JSON_RESPONSE = "response";
    public final String JSON_HEADERS = "headers";
    public final String JSON_WILL_INDEX = "willIndex";
    //有硬编码，只可以在后面添加
    public String[] jsonSegments = new String[]{JSON_REQUEST, JSON_RESPONSE, JSON_METHOD,
            JSON_URL, JSON_COMMENTS, JSON_HEADERS, JSON_WILL_INDEX};
}
