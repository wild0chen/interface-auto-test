package base.CaseCaller;

import base.enums.EnumRequestMethod;
import base.vo.BaseParameter2;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import com.alibaba.fastjson.JSONObject;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public class BaseCaseCaller implements ICaseCaller{
    public Logger logger = LogManager.getLogger();
    private String host = "";
    private BaseParameter2 baseParameter2;
    @Setter
    private String token;

    public BaseCaseCaller(String host, BaseParameter2 bp2){
        this.baseParameter2 = bp2;
        this.host = host;
    }

    public BaseCaseCaller(BaseParameter2 bp2){
        this.baseParameter2 = bp2;
    }

    public String call(String token){
        String response = call(EnumRequestMethod.getMethod(baseParameter2.getMethod()), baseParameter2.getUrl(), token, baseParameter2.getRequest().getInnerMap(), baseParameter2.getHeaders());
        return response;
    }

    @Override
    public void setResponse(String response){
        baseParameter2.setHttpResponse(response);
    }

    public String call(EnumRequestMethod method, String url, String accessToken,
                       Map<String, Object> map, Map<String, String> headers, Object... objs) {

//        if (useUnitHost)
//            url = configureProperty.getHostUrl() + url;
        logger.info(method + ":" + url + " \n  " + accessToken);
        String responseBody = "";
        switch (method) {
            case GET:
                responseBody = get(url, accessToken, map, headers);
                break;
            case POST:
                responseBody = post(url, accessToken, map, headers);
                break;
            default:
                logger.info("不是POST/GET方式");
                return responseBody;
        }
        return responseBody;
    }

    @Override
    public BaseParameter2 getBaseParameter(){
        return baseParameter2;
    }

    @Override
    public void dataExchangeFromPre(String resBody, BaseParameter2 preParam) {}

    @Override
    public int getWillIndex(){
        return baseParameter2.getWillIndex();
    }


    protected String post(String url, String accessToken, Map<String, Object> map, Map<String, String> headers) {

        return HttpRequest.post(url).addHeaders(headers)
                .cookie(accessToken).form(map).execute().body();
//        String raw = (String)map.get("__raw__");
//        return HttpRequest.post(url).addHeaders(headers)
//                .cookie(accessToken).form(map).body(raw).execute().body();
    }

    protected String get(String url, String accessToken, Map<String, Object> map, Map<String, String> headers) {
        String raw = (String)map.get("__raw__");
        return HttpRequest.get(url).addHeaders(headers).cookie(accessToken).form(map).body(raw).execute().body();
    }

    @Override
    public boolean judgeSuccess(String result) {
        JSONObject expected = baseParameter2.getExpected();
        if (StrUtil.isEmpty(result))
            return false;
        JSONObject response = JSONObject.parseObject(result);
        String code = "code";
        return expected.getInteger(code).equals(response.getInteger(code));
    }

    // 在调用前对参数的调整如加入验签
    @Override
    public void doSomethingBeforeCall(){
        doSomethingBeforeCall(baseParameter2.getRequest().getInnerMap());
    }
    public void doSomethingBeforeCall(Map<String, Object> map){}
    // 如在调用后需要解密啥的呢
    @Override
    public String doSomethingAfterCall(String response){return response;};

}
