package base.CaseCaller;

import base.vo.BaseParameter2;

//
public interface ICaseCaller {
    String call(String token);
    boolean judgeSuccess(String result);
    //与另外一个接口数据进行交换
    void dataExchangeFromPre(String resBody, BaseParameter2 preParam);

    BaseParameter2 getBaseParameter();
    void setResponse(String str);
    int getWillIndex();

    // 在调用前对参数的调整如加入验签
    void doSomethingBeforeCall();
    // 如在调用后需要解密啥的呢
    String doSomethingAfterCall(String response);
}
