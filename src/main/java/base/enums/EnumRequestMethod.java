package base.enums;

public enum EnumRequestMethod {
    GET(0, "get"), POST(1, "post");
    private int index;
    private String desc;
    EnumRequestMethod(int a, String s){
        index = a;
        desc = s;
    }

    public String getDesc(){
        return desc;
    }

    public static EnumRequestMethod getMethod(String key){
        for (EnumRequestMethod r : EnumRequestMethod.values()){
            if (r.getDesc().equals(key.toLowerCase())){
                return r;
            }
        }
        return null;
    }
}
