package calcIncome.testsuites;

import base.originBase.BaseParameter;
import base.originBase.BaseTemplate;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class CalcIncome extends BaseTemplate {

    @BeforeTest
    public void initialize() {
    }

    @Test(dataProvider = "data")
    @Override
    public void testCase(String comment, List<BaseParameter> paramterList) {
        doIt(paramterList, comment);
        if (getStepResults().size() != 2)
            logger.error("step的数量不对，没有获取全部的数据");
        //基金持仓详情页
        String day = getStepResults().get(0);
        //基金净值列表4个数
        String nav = getStepResults().get(1);
        JSONObject dayJson = JSON.parseObject(day);
        JSONObject navJson = JSON.parseObject(nav);
        List navData = (List) ((JSONObject) navJson.get("data")).get("records");
        BigDecimal dayInc1 = ((JSONObject) (navData.get(0))).getBigDecimal("fNavUnit");
        BigDecimal dayInc2 = ((JSONObject) (navData.get(1))).getBigDecimal("fNavUnit");
        //2日净值差
        BigDecimal disDayNavUnit = dayInc1.subtract(dayInc2);
        // 份额
        BigDecimal holdingShare = ((JSONObject) (dayJson.get("data"))).getBigDecimal("holdingShare");
        int incomeScale = 2;
        //接口昨日
        BigDecimal testYesterdayIncome = ((JSONObject) (dayJson.get("data"))).getBigDecimal("lastDayProfit");
        //昨日收益
        BigDecimal yesterdayIncome = disDayNavUnit.multiply(holdingShare);

        //接口日涨幅
        BigDecimal testDailyIncrement = ((JSONObject) (dayJson.get("data"))).getBigDecimal("dailyIncrement");
        // 昨日涨幅
        BigDecimal yesterdayIncrement = disDayNavUnit.divide(dayInc2, RoundingMode.HALF_UP).multiply(new BigDecimal(100));

        //持仓金额计算
        //持有金额
        BigDecimal holdingAsset = ((JSONObject) (dayJson.get("data"))).getBigDecimal("holdingAsset");
        //在途
        BigDecimal onWayAsset = ((JSONObject) (dayJson.get("data"))).getBigDecimal("onWayAsset");
        // 总金额
        BigDecimal totalAsset = ((JSONObject) (dayJson.get("data"))).getBigDecimal("totalAsset");

        //基金净值
        //昨日净值
        String pernetValue = ((JSONObject) dayJson.get("data")).getString("pernetValue");
        String testValue = ((JSONObject) (navData.get(0))).getString("fNavUnit");

        boolean allTest = true;
        //昨日收益一定满足
        boolean isPass = testIncome(yesterdayIncome, testYesterdayIncome, incomeScale);
        logger.info("收益测试：" + isPass);
        allTest = isPass && allTest;
        // 昨日涨幅一定满足
        isPass = testIncome(yesterdayIncrement, testDailyIncrement, incomeScale);
        logger.info("日涨幅测试：" + isPass);
        allTest = isPass && allTest;
        // 总收益
        isPass = totalAsset.compareTo(holdingAsset.add(onWayAsset)) == 0;
        logger.info("总收益测试：" + isPass);
        allTest = isPass && allTest;
        //净值比较
        isPass = pernetValue.equals(testValue);
        logger.info("净值测试: " + isPass);
        allTest = isPass && allTest;

        Assert.assertEquals(allTest, true);
    }

    //比较±0.01
    boolean testIncome(BigDecimal calc, BigDecimal test, int scale) {
        //精度
        BigDecimal limit = new BigDecimal(0.1);
        for (int i = 0; i < scale; ++i)
            limit = limit.multiply(limit);
        limit.setScale(scale, BigDecimal.ROUND_HALF_UP);

        BigDecimal distance = test.setScale(scale, BigDecimal.ROUND_HALF_UP).subtract(calc.setScale(scale, BigDecimal.ROUND_HALF_UP));
        boolean ret = distance.compareTo(limit) <= 0 && distance.compareTo(new BigDecimal(-0.01)) >= 0;
        logger.info(calc.setScale(scale, BigDecimal.ROUND_HALF_UP).toString() + " CMP " + test.setScale(scale, BigDecimal.ROUND_HALF_UP).toString() + " >>> " + ret);
        return ret;
    }

    @Override
    public void initToken(String token) {
        setToken("access-token=\"eyJtYW1VIjoxNywibWFtVCI6IlpXUmhZbU00WXpGbVlUTTBOR0pqTVRFeFpqQXpPR0UyT0daa05UUmtPVFpqTm1JME5qSXdOMkU1WVdVeU0yWmlaVGcxTVRka016RXhNalkwTWpFM1pUWTNOR0k1T1RGaFlURTVPV1l3TlRRMk5HWXpNemd4WW1NMVpUazJPVE0xWVdFd1pqTTBNekEzT0RreU5ETm1NV1UxWkdaaU4yRm1OekU0TVRreFlUZz0iLCJtYW1DIjoiMDAxIiwiYWNjZXNzVG9rZW4iOm51bGwsImN1c3RvbWVyRGV0YWlsUmVzcCI6bnVsbH0=\"");
    }
}
