package timerUpdate.testsuites;

import base.originBase.BaseParameter;
import base.originBase.BaseTemplate;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class TimerUpdate extends BaseTemplate {

    @BeforeTest
    public void initialize() {
    }

    @Test(dataProvider = "data")
    @Override
    public void testCase(String comment, List<BaseParameter> paramterList) {
        while(true) {
            doIt(paramterList, comment);
            String res = getStepResults().get(0);
            JSONObject obj = paramterList.get(0).getResponse();
            if (endFor(res, obj) || isTimeout()){
                logger.info("结束>>>最后的结果为：" + res);
                break;
            }

            System.out.println(new Date());
            try {
                Thread.sleep(60 * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean isTimeout(){
        Date d = new Date();
        if (d.getDate() >= 16)
            return true;
        return false;
    }

    protected boolean endFor(String result, JSONObject expected) {
        if (StrUtil.isEmpty(result))
            return false;
        JSONObject response = JSONObject.parseObject(result);

        String time1 = response.getJSONObject("data").getJSONArray("profits").getJSONObject(0).getString("date");
        String time2 = expected.getJSONObject("data").getJSONArray("profits").getString(0);
        if (time1.equals(time2)) {
            String res =response.getJSONObject("data").getJSONArray("profits").getJSONObject(0).getString("amount");
            logger.info(res);
            return true;
        }

        return false;
    }

    //比较±0.01
    boolean testIncome(BigDecimal calc, BigDecimal test, int scale) {
        //精度
        BigDecimal limit = new BigDecimal(0.1);
        for (int i = 0; i < scale; ++i)
            limit = limit.multiply(limit);
        limit.setScale(scale, BigDecimal.ROUND_HALF_UP);

        BigDecimal distance = test.setScale(scale, BigDecimal.ROUND_HALF_UP).subtract(calc.setScale(scale, BigDecimal.ROUND_HALF_UP));
        boolean ret = distance.compareTo(limit) <= 0 && distance.compareTo(new BigDecimal(-0.01)) >= 0;
        logger.info(calc.setScale(scale, BigDecimal.ROUND_HALF_UP).toString() + " CMP " + test.setScale(scale, BigDecimal.ROUND_HALF_UP).toString() + " >>> " + ret);
        return ret;
    }

    @Override
    public void initToken(String token) {
        setToken("access-token=\"eyJtYW1VIjo2MCwibWFtVCI6Ik5XRmhabVl3TURkaE16QXhNMkV3TlRZNVl6QTBOREF3WkRreVlXRmlPV0UyTjJFMU1XUm1ORFJsTmpWa04ySmhOekEzWXprek5qaGlZbUZoWVRnMU1XWmhObVl6TWpNeE1tSTRZVE0zWkRreFpUSXlNRFkxTmpZMVlqQmpNVGM1WldVd016STBNamxoWXpNell6VmtZemxoTVdRME1XWmlOV1kzTVRFMU1URT0iLCJtYW1DIjoiMDAxIiwiYWNjZXNzVG9rZW4iOm51bGwsImN1c3RvbWVyRGV0YWlsUmVzcCI6bnVsbH0=\"");
    }
}
