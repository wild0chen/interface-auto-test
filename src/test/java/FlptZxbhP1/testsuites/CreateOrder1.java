package FlptZxbhP1.testsuites;

import base.CaseCaller.ICaseCaller;
import base.token.AbstractFetchToken;
import base.token.FLPTFetchToken;
import base.vo.BaseParameter2;
import base.TemplateUse.BaseTemplate2;
import com.alibaba.fastjson.JSONObject;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import utils.ConfigureProperty;

import java.util.Date;
import java.util.List;

public class CreateOrder1 extends BaseTemplate2 {

    @BeforeTest
    public void initialize() {
    }

    @Test(dataProvider = "data")
    @Override
    public void testCase(String comment, List<BaseParameter2> paramterList) {
        List<ICaseCaller> caseCallers = caseCallerListFactory(paramterList);
        doIt(caseCallers, comment);
        String res = getStepResults().get(0);
    }

    private void showTarget(String res){
    }

    @Override
    public void createToken() {
        ConfigureProperty cfg = new ConfigureProperty("zhuzhanghao", "defaultHost");
        this.setFetchToken(new FLPTFetchToken(cfg.getAccount()));
    }
}
