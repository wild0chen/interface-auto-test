package online.testsuites;

import base.originBase.BaseParameter;
import base.originBase.BaseTemplate;
import org.testng.annotations.Test;

import java.util.List;

public class Apply extends BaseTemplate {

    @Override
    @Test(dataProvider = "data")
    public void testCase(String comment, List<BaseParameter> parameterList) {
        doIt(parameterList, comment);
    }

    @Override
    public void initToken(String token){
        setToken("access-token=\"eyJtYW1VIjoxNywibWFtVCI6IlpXUmhZbU00WXpGbVlUTTBOR0pqTVRFeFpqQXpPR0UyT0daa05UUmtPVFpqTm1JME5qSXdOMkU1WVdVeU0yWmlaVGcxTVRka016RXhNalkwTWpFM1pUWTNOR0k1T1RGaFlURTVPV1l3TlRRMk5HWXpNemd4WW1NMVpUazJPVE0xWVdFd1pqTTBNekEzT0RreU5ETm1NV1UxWkdaaU4yRm1OekU0TVRreFlUZz0iLCJtYW1DIjoiMDAxIiwiYWNjZXNzVG9rZW4iOm51bGwsImN1c3RvbWVyRGV0YWlsUmVzcCI6bnVsbH0=");
    }
}
