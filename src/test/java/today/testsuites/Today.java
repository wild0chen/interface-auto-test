package today.testsuites;

import base.originBase.BaseParameter;
import base.originBase.BaseTemplate;
import com.alibaba.fastjson.JSONObject;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.Date;
import java.util.List;

public class Today extends BaseTemplate {

    @BeforeTest
    public void initialize() {
    }

    @Test(dataProvider = "data")
    @Override
    public void testCase(String comment, List<BaseParameter> paramterList) {
        while(true) {
            doIt(paramterList, comment);
            String res = getStepResults().get(0);
            showTarget(res);
            if (isTimeout()){
                logger.info("结束>>>最后的结果为：" + res);
                break;
            }
            System.out.println(new Date());

            try {
                Thread.sleep(60 * 10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void showTarget(String res){
        JSONObject jsonObject = JSONObject.parseObject(res).getJSONObject("data");
        String curDay = jsonObject.getString("curDay");
        String curDayProfit = jsonObject.getString("curDayProfit");
        String lastDay = jsonObject.getString("lastDay");
        String lastDayProfit = jsonObject.getString("lastDayProfit");
        logger.info(">>>>> curDay: " + curDay + " curProfit: " + curDayProfit);
        logger.info(">>>>> lastDay: " + lastDay + " lastProfit: " + lastDayProfit);
    }

    private boolean isTimeout(){
        Date d = new Date();
        if (d.getDate() >= 17)
            return true;
        return false;
    }

    @Override
    public void initToken(String token) {
        setToken("access-token=\"eyJtYW1VIjo2MCwibWFtVCI6Ik16VTJPVFJtWW1ObU56Vm1aalUzT0Roak5UY3lOalJpTlRjNE16Z3pNVGhpWVdWaU1tTTJNVFkwTmpGa01EVTBPR1pqWW1ZeU9XTXdOREU1TWpneFl6QmpZemxqWWprNFpEZ3dPR1F4TmpSa01qWm1NMkl5TnpNek5HUmpZbU01Tm1VeFlqVTFaakZrWWpaaU5UZG1Nek5pTjJObVlqSm1aVEF6WWpCak1HST0iLCJtYW1DIjoiMDAxIiwiYWNjZXNzVG9rZW4iOm51bGwsImN1c3RvbWVyRGV0YWlsUmVzcCI6bnVsbH0=\"");
    }
}
