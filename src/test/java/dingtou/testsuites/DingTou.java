package dingtou.testsuites;

import base.originBase.BaseParameter;
import base.originBase.BaseTemplate;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.List;

public class DingTou extends BaseTemplate {

    @BeforeTest
    public void initialize() {
    }

    @Test(dataProvider = "data")
    @Override
    public void testCase(String comment, List<BaseParameter> paramterList) {
//        doIt(paramterList, comment);

        String cot = "access-token=\"eyJtYW1VIjo2MCwibWFtVCI6Ik5XRmhabVl3TURkaE16QXhNMkV3TlRZNVl6QTBOREF3WkRreVlXRmlPV0UyTjJFMU1XUm1ORFJsTmpWa04ySmhOekEzWXprek5qaGlZbUZoWVRnMU1XWmhObVl6TWpNeE1tSTRZVE0zWkRreFpUSXlNRFkxTmpZMVlqQmpNVGM1WldVd016STBNamxoWXpNell6VmtZemxoTVdRME1XWmlOV1kzTVRFMU1URT0iLCJtYW1DIjoiMDAxIiwiYWNjZXNzVG9rZW4iOm51bGwsImN1c3RvbWVyRGV0YWlsUmVzcCI6bnVsbH0=\"";

        String sb = "{\"outflowno\": \"1234\",\"businesstype\": \"00\",\"tradeacco\": \"4302\",\"fundcode\": \"180001\",\"sharetype\": \"A\", \"bankacco\": \"6226300512349899\", \"applysum\": \"100\", \"cycleunit\": \"2\", \"jyzq\": \"1\", \"jyrq\": \"02\", \"zzrq\": \"99991231\", \"scjyrq\": \"000000\", \"returnurl\": \"http://www.baidu.com\", \"channelid\": \"001\", \"identityno\": \"420104198801310857\", \"identitytype\": \"0\" }";
        String s = HttpRequest.post("http://fund-web-admin.fund-ops-test.paas.test/api/mam-trade/mam/createFixedInvestmentAgreement")
//                .header("Content-Type", "application/json")
                .cookie(cot).body(sb).execute().body();
        System.out.println(s);
    }

    @Override
    protected String doSomethingAfterCall(String response) {
        if (StrUtil.isEmpty(response))
            return "";
        return response;
    }

    @Override
    public void initToken(String token) {
        setToken("access-token=\"eyJtYW1VIjo2MCwibWFtVCI6Ik5XRmhabVl3TURkaE16QXhNMkV3TlRZNVl6QTBOREF3WkRreVlXRmlPV0UyTjJFMU1XUm1ORFJsTmpWa04ySmhOekEzWXprek5qaGlZbUZoWVRnMU1XWmhObVl6TWpNeE1tSTRZVE0zWkRreFpUSXlNRFkxTmpZMVlqQmpNVGM1WldVd016STBNamxoWXpNell6VmtZemxoTVdRME1XWmlOV1kzTVRFMU1URT0iLCJtYW1DIjoiMDAxIiwiYWNjZXNzVG9rZW4iOm51bGwsImN1c3RvbWVyRGV0YWlsUmVzcCI6bnVsbH0=\"");
    }
}
