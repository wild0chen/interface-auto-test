package login.testsuites;

import base.originBase.BaseParameter;
import base.originBase.BaseTemplate;
import base.enums.EnumRequestMethod;
import com.alibaba.fastjson.JSONObject;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

public class TestLogin extends BaseTemplate {

    @BeforeTest
    public void initialize(){
    }

    private static boolean analyzerStep1(String responseBody, BaseParameter parameter){
        JSONObject resJson = JSONObject.parseObject(responseBody);
        String key = "singleOrderOffsetId";
        String offsetId = resJson.getJSONObject("data").getString(key);
        parameter.getRequest().getInnerMap().put(key, offsetId);
        return true;
    }

    @Override
    protected String call(EnumRequestMethod method, String url, String accessToken, Map<String, Object> map, Map<String, String> header, Object... objs) {
        return  "{\"code\":200 , \"data\":{ \"singleOrderOffsetId\" : 123456}}";
    }
//
//    @Override
//    final boolean judgeSuccess(String result, JSONObject expected) {
//        return true;
//    }

    @Test(dataProvider = "data")
    @Override
    public void testCase(String comment, List<BaseParameter> paramterList){
        List<BiFunction<String, BaseParameter, Boolean>> list = new ArrayList<>();
        list.add(TestLogin::analyzerStep1);
        list.add(BaseTemplate::analyzerNullStep);
        doIt(paramterList, comment, list);
    }
}
